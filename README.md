# Python file system image reader

* read image files and extract information.
* extract files and directories, save them with original time stamp
* read only classical generic MBR and FAT 12/16/32 images.

## Using
```
fs_reader.py [-h] {mbr,fat}
```
### MBR
```
fs_reader.py mbr [-h] file

positional arguments:
  file        image file

optional arguments:
  -h, --help  show this help message and exit
```
### FAT 12/16/32
```
fs_reader.py fat [-h] [--log [LOG]] [--out [OUT]] [--offset OFFSET]
                 [--verbose {1,2,3}] [--version] file

positional arguments:
  file                          imagefile

optional arguments:
  -h, --help                    show this help message and exit
  --log [LOG], -l [LOG]         log file
  --out [OUT], -o [OUT]         save files/directories to OUT directory
  --offset OFFSET, -O OFFSET    offset to the Image
  --verbose {1,2,3}, -v {1,2,3} increase output verbosity
  --version, -V                 show program's version number and exit
```

## TODO
* test and fix errors by reading FAT32
* read more file systems
