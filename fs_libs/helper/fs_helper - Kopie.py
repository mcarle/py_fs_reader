#!/usr/bin/env python
# -*- coding: UTF-8 -*-

SECT_SIZE = 512

# structs
U_SHORT_LIT = '<H'  # unsigned short little-endian 2 byte
U_CHAR_LIT = '<B'  # unsigned char little-endian 1 byte
U_INT_LIT = '<I'  # unsigned int little-endian 4 byte
U_LONG_LIT = '<L'  # unsigned long little-endian 4 byte
def char_array_lit(x): return '<{}s'.format(x)  # char[] little-endian

# YES = 'JA'
# NO = 'NEIN'
YES = 'YES'
NO = 'NO'

IMG_FILE_ERR = 'file error with image file'
LOG_FILE_ERR = 'file error with log file'

# --------------MBR--------------
# MBR_IDF_ERR = 'Paritionstyp nicht erkannt'
MBR_IDF_ERR = 'partition type not identified'

# http://www.win.tue.nl/~aeb/partitions/partition_types-1.html
PART_TYPES = {
    0x00: 'Empty',
    0x01: 'DOS 12-bit FAT',
    0x02: 'XENIX root',
    0x03: 'XENIX /usr',
    0x04: 'DOS 3.0+ 16-bit FAT (up to 32M)',
    0x05: 'DOS 3.3+ Extended Partition',
    0x06: 'DOS 3.31+ 16-bit FAT (over 32M)',
    0x07: 'OS/2 IFS (e.g., HPFS)|NTFS|exFAT|Advanced Unix|QNX2.x pre-1988 (see below under IDs 4d-4f)',
    0x08: 'OS/2 (v1.0-1.3 only)|AIX boot partition|SplitDrive|Commodore DOS|DELL partition spanning multiple drives|QNX 1.x and 2.x (qny)',
    0x09: 'AIX data partition|Coherent filesystem|QNX 1.x and 2.x (qnz)',
    0x0a: 'OS/2 Boot Manager|Coherent swap partition|OPUS',
    0x0b: 'WIN95 OSR2 FAT32',
    0x0c: 'WIN95 OSR2 FAT32, LBA-mapped',
    0x0d: 'SILICON SAFE',
    0x0e: 'WIN95: DOS 16-bit FAT, LBA-mapped',
    0x0f: 'WIN95: Extended partition, LBA-mapped',
    0x10: 'OPUS (?)',
    0x11: 'Hidden DOS 12-bit FAT or Leading Edge DOS 3.x logically sectored FAT',
    0x12: 'Configuration/diagnostics partition',
    0x14: 'Hidden DOS 16-bit FAT <32M|AST DOS with logically sectored FAT',
    0x16: 'Hidden DOS 16-bit FAT >=32M',
    0x17: 'Hidden IFS (e.g., HPFS)',
    0x18: 'AST SmartSleep Partition',
    0x19: 'Unused',
    0x1b: 'Hidden WIN95 OSR2 FAT32',
    0x1c: 'Hidden WIN95 OSR2 FAT32, LBA-mapped',
    0x1e: 'Hidden WIN95 16-bit FAT, LBA-mapped',
    0x20: 'Unused',
    0x21: 'Reserved|Unused',
    0x22: 'Unused',
    0x23: 'Reserved',
    0x24: 'NEC DOS 3.x',
    0x26: 'Reserved',
    0x27: 'PQservice or Windows RE hidden partition|MirOS partition|RouterBOOT kernel partition',
    0x2a: 'AtheOS File System (AFS)',
    0x2b: 'SyllableSecure (SylStor)',
    0x31: 'Reserved',
    0x32: 'NOS',
    0x33: 'Reserved',
    0x34: 'Reserved',
    0x35: 'JFS on OS/2 or eCS',
    0x36: 'Reserved',
    0x38: 'THEOS ver 3.2 2gb partition',
    0x39: 'Plan 9 partition|THEOS ver 4 spanned partition',
    0x3a: 'THEOS ver 4 4gb partition',
    0x3b: 'THEOS ver 4 extended partition',
    0x3c: 'PartitionMagic recovery partition',
    0x3d: 'Hidden NetWare',
    0x40: 'Venix 80286 or PICK',
    0x41: 'Linux/MINIX (sharing disk with DRDOS)|Personal RISC Boot|PPC PReP (Power PC Reference Platform) Boot',
    0x42: 'Linux swap (sharing disk with DRDOS)|SFS (Secure Filesystem)|Windows 2000 dynamic extended partition marker',
    0x43: 'Linux native (sharing disk with DRDOS)',
    0x44: 'GoBack partition',
    0x45: 'Boot-US boot manager|Priam|EUMEL/Elan',
    0x46: 'EUMEL/Elan',
    0x47: 'EUMEL/Elan',
    0x48: 'EUMEL/Elan',
    0x4a: 'Mark Aitchisons ALFS/THIN lightweight filesystem for DOS|AdaOS Aquila (Withdrawn)',
    0x4c: 'Oberon partition',
    0x4d: 'QNX4.x',
    0x4e: 'QNX4.x 2nd part',
    0x4f: 'QNX4.x 3rd part|Oberon partition',
    0x50: 'OnTrack Disk Manager (older versions) RO|Lynx RTOS|Native Oberon (alt)',
    0x51: 'OnTrack Disk Manager RW (DM6 Aux1) or Novell',
    0x52: 'CP/M|Microport SysV/AT',
    0x53: 'Disk Manager 6.0 Aux3',
    0x54: 'Disk Manager 6.0 Dynamic Drive Overlay (DDO)',
    0x55: 'EZ-Drive',
    0x56: 'Golden Bow VFeature Partitioned Volume|DM converted to EZ-BIOS|AT&T MS-DOS 3.x logically sectored FAT',
    0x57: 'DrivePro|VNDI Partition',
    0x5c: 'Priam EDisk',
    0x61: 'SpeedStor',
    0x63: 'Unix System V (SCO, ISC Unix, UnixWare, ...), Mach, GNU Hurd',
    0x64: 'PC-ARMOUR protected partition|Novell Netware 286, 2.xx',
    0x65: 'Novell Netware 386, 3.xx or 4.xx',
    0x66: 'Novell Netware SMS Partition',
    0x67: 'Novell',
    0x68: 'Novell',
    0x69: 'Novell Netware 5+, Novell Netware NSS Partition',
    0x6e: '??',
    0x70: 'DiskSecure Multi-Boot',
    0x71: 'Reserved',
    0x72: 'V7/x86',
    0x73: 'Reserved',
    0x74: 'Reserved|Scramdisk partition',
    0x75: 'IBM PC/IX',
    0x76: 'Reserved',
    0x77: 'M2FS/M2CS partition|VNDI Partition',
    0x78: 'XOSL FS',
    0x7e: 'Unused',
    0x7f: 'Unused',
    0x80: 'MINIX until 1.4a',
    0x81: 'MINIX since 1.4b, early Linux|Mitac disk manager',
    0x82: 'Prime or Solaris x86|Linux swap',
    0x83: 'Linux native partition',
    0x84: 'OS/2 hidden C: drive|Hibernation partition',
    0x85: 'Linux extended partition',
    0x86: 'Old Linux RAID partition superblock|FAT16 volume set',
    0x87: 'NTFS volume set',
    0x88: 'Linux plaintext partition table',
    0x8a: 'Linux Kernel Partition (used by AiR-BOOT)',
    0x8b: 'Legacy Fault Tolerant FAT32 volume',
    0x8c: 'Legacy Fault Tolerant FAT32 volume using BIOS extd INT 13h',
    0x8d: 'Free FDISK 0.96+ hidden Primary DOS FAT12 partitition',
    0x8e: 'Linux Logical Volume Manager partition',
    0x90: 'Free FDISK 0.96+ hidden Primary DOS FAT16 partitition',
    0x91: 'Free FDISK 0.96+ hidden DOS extended partitition',
    0x92: 'Free FDISK 0.96+ hidden Primary DOS large FAT16 partitition',
    0x93: 'Hidden Linux native partition|Amoeba',
    0x94: 'Amoeba bad block table',
    0x95: 'MIT EXOPC native partitions',
    0x96: 'CHRP ISO-9660 filesystem',
    0x97: 'Free FDISK 0.96+ hidden Primary DOS FAT32 partitition',
    0x98: 'Free FDISK 0.96+ hidden Primary DOS FAT32 partitition (LBA)|Datalight ROM-DOS Super-Boot Partition',
    0x99: 'DCE376 logical drive',
    0x9a: 'Free FDISK 0.96+ hidden Primary DOS FAT16 partitition (LBA)',
    0x9b: 'Free FDISK 0.96+ hidden DOS extended partitition (LBA)',
    0x9e: 'ForthOS partition',
    0x9f: 'BSD/OS',
    0xa0: 'Laptop hibernation partition',
    0xa1: 'Laptop hibernation partition|HP Volume Expansion (SpeedStor variant)',
    0xa3: 'HP Volume Expansion (SpeedStor variant)',
    0xa4: 'HP Volume Expansion (SpeedStor variant)',
    0xa5: 'BSD/386, 386BSD, NetBSD, FreeBSD',
    0xa6: 'OpenBSD|HP Volume Expansion (SpeedStor variant)',
    0xa7: 'NeXTStep',
    0xa8: 'Mac OS-X',
    0xa9: 'NetBSD',
    0xaa: 'Olivetti Fat 12 1.44MB Service Partition',
    0xab: 'Mac OS-X Boot partition|GO! partition',
    0xad: 'RISC OS ADFS',
    0xae: 'ShagOS filesystem',
    0xaf: 'MacOS X HFS',
    0xb0: 'BootStar Dummy',
    0xb1: 'HP Volume Expansion (SpeedStor variant)|QNX Neutrino Power-Safe filesystem',
    0xb2: 'QNX Neutrino Power-Safe filesystem',
    0xb3: 'HP Volume Expansion (SpeedStor variant)|QNX Neutrino Power-Safe filesystem',
    0xb4: 'HP Volume Expansion (SpeedStor variant)',
    0xb6: 'HP Volume Expansion (SpeedStor variant)|Corrupted Windows NT mirror set (master), FAT16 file system',
    0xb7: 'Corrupted Windows NT mirror set (master), NTFS file system|BSDI BSD/386 filesystem',
    0xb8: 'BSDI BSD/386 swap partition',
    0xbb: 'Boot Wizard hidden',
    0xbc: 'Acronis backup partition',
    0xbd: 'BonnyDOS/286',
    0xbe: 'Solaris 8 boot partition',
    0xbf: 'New Solaris x86 partition',
    0xc0: 'CTOS|REAL/32 secure small partition|NTFT Partition|DR-DOS/Novell DOS secured partition',
    0xc1: 'DRDOS/secured (FAT-12)',
    0xc2: 'Unused|Hidden Linux',
    0xc3: 'Hidden Linux swap',
    0xc4: 'DRDOS/secured (FAT-16, < 32M)',
    0xc5: 'DRDOS/secured (extended)',
    0xc6: 'DRDOS/secured (FAT-16, >= 32M)|Windows NT corrupted FAT16 volume/stripe set',
    0xc7: 'Windows NT corrupted NTFS volume/stripe set|Syrinx boot',
    0xc8: 'Reserved for DR-DOS 8.0+',
    0xc9: 'Reserved for DR-DOS 8.0+',
    0xca: 'Reserved for DR-DOS 8.0+',
    0xcb: 'DR-DOS 7.04+ secured FAT32 (CHS)/',
    0xcc: 'DR-DOS 7.04+ secured FAT32 (LBA)/',
    0xcd: 'CTOS Memdump?',
    0xce: 'DR-DOS 7.04+ FAT16X (LBA)/',
    0xcf: 'DR-DOS 7.04+ secured EXT DOS (LBA)/',
    0xd0: 'REAL/32 secure big partition|Multiuser DOS secured partition',
    0xd1: 'Old Multiuser DOS secured FAT12',
    0xd4: 'Old Multiuser DOS secured FAT16 <32M',
    0xd5: 'Old Multiuser DOS secured extended partition',
    0xd6: 'Old Multiuser DOS secured FAT16 >=32M',
    0xd8: 'CP/M-86',
    0xda: 'Non-FS Data|Powercopy Backup',
    0xdb: 'Digital Research CP/M, Concurrent CP/M, Concurrent DOS|CTOS|KDG Telemetry SCPU boot',
    0xdd: 'Hidden CTOS Memdump?',
    0xde: 'Dell PowerEdge Server utilities (FAT fs)',
    0xdf: 'DG/UX virtual disk manager partition|BootIt EMBRM',
    0xe0: 'Reserved by STMicroelectronics for a filesystem called ST AVFS',
    0xe1: 'DOS access or SpeedStor 12-bit FAT extended partition',
    0xe3: 'DOS R/O or SpeedStor',
    0xe4: 'SpeedStor 16-bit FAT extended partition < 1024 cyl.',
    0xe5: 'Tandy MSDOS with logically sectored FAT',
    0xe6: 'Storage Dimensions SpeedStor',
    0xe8: 'LUKS',
    0xeb: 'BeOS BFS',
    0xec: 'SkyOS SkyFS',
    0xed: 'Unused',
    0xee: 'Indication that this legacy MBR is followed by an EFI header',
    0xef: 'Partition that contains an EFI file system',
    0xf0: 'Linux/PA-RISC boot loader',
    0xf1: 'Storage Dimensions SpeedStor',
    0xf2: 'DOS 3.3+ secondary partition',
    0xf3: 'Reserved',
    0xf4: 'SpeedStor large partition|Prologue single-volume partition',
    0xf5: 'Prologue multi-volume partition',
    0xf6: 'Storage Dimensions SpeedStor',
    0xf7: 'DDRdrive Solid State File System',
    0xf9: 'pCache',
    0xfa: 'Bochs',
    0xfb: 'VMware File System partition',
    0xfc: 'VMware Swap partition',
    0xfd: 'Linux raid partition with autodetect using persistent superblock',
    0xfe: 'SpeedStor > 1024 cyl.|LANstep|IBM PS/2 IML|Windows NT Disk Administrator hidden partition|Linux Logical Volume Manager partition (old)',
    0xff: 'Xenix Bad Block Table'}

# offsets
MBR_BOOTLOADER_OFFSET = 0x00
MBR_BOOTLOADER_SIZE = 440
MBR_DISK_SIG_OFFSET = 0x1B8
MBR_RAW_PART_TABLE_OFFSET = 0x1BE
MBR_RAW_PART_TABLE_SIZE = 64

BOOTEBLE_OFFSET = 0x00
CHS_FIRST_OFFSET = 0x01
CHS_FIRST_SIZE = 3
PART_TYP_OFFSET = 0x04
CHS_LAST_OFFSET = 0x05
CHS_LAST_SIZE = 3
START_SECT_OFFSET = 0x08
NUM_SECT_OFFSET = 0x0C

# OUT_MBR = '''MBR
 # MBR-Signatur:          0x{mbr_sig:X}
 # Datentraegersignatur:  0x{disk_sig:X}
 # Partitionstabelle:
  # -----------------------------------------------------------------
# '''
OUT_MBR = '''Classical generic MBR
MBR signature:          0x{mbr_sig:X}
Disk signature:         0x{disk_sig:X}
Partition Table:
-----------------------------------------------------------------
'''

# OUT_PARTT = '''  Partitionstyp:                   {part_typ}
  # bootfaehig:                      {boot}
  # CHS-Eintrag des ersten Sektors:  {chs_first}
  # CHS-Eintrag des letzten Sektors: {chs_last}
  # Startsektor:                     {start_sect}
  # Anzahl der Sektoren in der
  # Partition per LBA-Methode:       {lba_met}
  # -----------------------------------------------------------------
# '''
OUT_PARTT = '''  Partitionstyp:                    {part_typ}
  bootfaehig:                       {boot}
  CHS address first sector:         {chs_first}
  CHS address last sector:          {chs_last}
  Start sector:                     {start_sect}
  Number of sectors in partition:   {lba_met}
-----------------------------------------------------------------
'''

# --------------FAT--------------

# differend fat versions
HEX_DIGITS_12 = 3
FREE_CLUSTER_12 = 0x000
RSV_INTERN_12 = 0x001
RSV_DO_NOT_USE_12 = 0xFF6
BAD_SEKTOR_12 = 0xFF7

HEX_DIGITS_16 = 4
FREE_CLUSTER_16 = 0x0000
RSV_INTERN_16 = 0x0001
RSV_DO_NOT_USE_16 = 0xFFF6
BAD_SEKTOR_16 = 0xFFF7

HEX_DIGITS_32 = 8
FREE_CLUSTER_32 = 0x00000000
RSV_INTERN_32 = 0x00000001
RSV_DO_NOT_USE_32 = 0x0FFFFFF6
BAD_SEKTOR_32 = 0x0FFFFFF

FAT32_28_BIT = 0x0FFFFFFF
# cluster types
DATA_CLUSTER_START_12 = 0x002
DATA_CLUSTER_END_12 = 0xFEF
DATA_CLUSTER_START_16 = 0x0002
DATA_CLUSTER_END_16 = 0xFFEF
DATA_CLUSTER_START_32 = 0x0000002
DATA_CLUSTER_END_32 = 0xFFFFFEF

RSV_CONTEXT_START_12 = 0xFF0
RSV_CONTEXT_END_12 = 0xFF5
RSV_CONTEXT_START_16 = 0xFFF0
RSV_CONTEXT_END_16 = 0xFFF5
RSV_CONTEXT_START_32 = 0xFFFFFFF0
RSV_CONTEXT_END_32 = 0xFFFFFFF5

LAST_CLUSTER_START_12 = 0xFF8
LAST_CLUSTER_END_12 = 0xFFF
LAST_CLUSTER_START_16 = 0xFFF8
LAST_CLUSTER_END_16 = 0xFFFF
LAST_CLUSTER_START_32 = 0xFFFFFF8
LAST_CLUSTER_END_32 = 0xFFFFFFF

# offsets
MACHINE_CODE_OFFSET = 0x00
OEM_NAME_OFFSET = 0x03
BYTES_PER_SECT_OFFSET = 0x0B
SECT_PER_CLUST_OFFSET = 0x0D
RSV_SECTS_OFFSET = 0x0E
FAT_COPIES_OFFSET = 0x10
DIR_ENTRIES_OFFSET = 0x11
TOTAL_SECTS_OFFSET = 0x13
MEDIA_DISKRP_BYTE_OFFSET = 0x15
SECTS_PER_FAT_OFFSET = 0x16
SECT_PER_TRACK_OFFSET = 0x18
NR_SIDES_OFFSET = 0x1A
HIDDEN_SECTS_OFFSET = 0x1C
LARGE_AMOUNT_SECTS_OFFSET = 0x20

# offsets 12/16
PHY_DRIVE_NR_OFFSET = 0x024
RSV_FAT12_16_OFFSET = 0x025
EXT_BOOT_SIG_OFFSET = 0x026
VOLM_ID_OFFSET = 0x027
PART_VOLM_LABLE_OFFSET = 0x02B
FAT_VARIANTE_OFFSET = 0x036
BOOT_CODE_OFFSET_START = 0x03E
BOOT_CODE_OFFSET_SIZE = 0x1C0
BOOT_CODE_OFFSET_END = BOOT_CODE_OFFSET_START + BOOT_CODE_OFFSET_SIZE
BIOS_BOOT_SIG_OFFSET = 0x1FE

# offsets 32
SECT_PER_FAT_OFFSET = 0x024
FAT_FLAGS_OFFSET = 0x028
FAT32_VER_OFFSET = 0x02A
ROOT_CLUSTER_NR_OFFSET = 0x02C
FSI_SECT_OFFSET = 0x030
SECT_NR_BOOT_SECT_COPY_OFFSET = 0x032
RSV_E_OFFSET_START = 0x034
RSV_E_OFFSET_SIZE = 0xC
RSV_E_OFFSET_END = RSV_E_OFFSET_START + RSV_E_OFFSET_SIZE
DRIVE_NR_OFFSET = 0x040
RSV_B_OFFSET = 0x041
EXT_BOOT_SIG_OFFSET = 0x042
FS_ID_OFFSET = 0x043
FS_NAME_OFFSET_START = 0x047
FS_NAME_OFFSET_SIZE = 0x00B
FS_NAME_OFFSET_END = FS_NAME_OFFSET_START + FS_NAME_OFFSET_SIZE
FAT_VER_OFFSET_START = 0x052
FAT_VER_OFFSET_SIZE = 0x008
FAT_VER_OFFSET_END = FAT_VER_OFFSET_START + FAT_VER_OFFSET_SIZE
BOOT_CODE_32_OFFSET_START = 0x05A
BOOT_CODE_32_OFFSET_SIZE = 0x1A4
BOOT_CODE_32_OFFSET_END = BOOT_CODE_32_OFFSET_START + BOOT_CODE_32_OFFSET_SIZE
BIOS_BOOTSEK_SIG_OFFSET = 0x1FE

# fat versions
FAT12 = 12
FAT16 = 16
FAT32 = 32

# max cluster size per file system typ
MAX_CLUSTER_12 = 4085
MAX_CLUSTER_16 = 65525

# fat12
HALF_24_BIT_LAST = 0x000FFF
HALF_24_BIT_FIRST = 0xFFF000
RIGHT_SHIFT_12_BIT = 12

# directory, first byte from filename
AVAILABLE = 0x00
DOT_ENTRY = 0x2E  # Dot entry (. or ..)
DELETED = 0xE5

# encodings
ENC_CP850 = 'cp850'
ENC_UTF_16 = 'utf_16'
ENC_UTF8 = 'utf_8'
ENC_ERRORS = 'ignore'

# VFAT
BIT_0_TO_5 = 0x1F
BIT_6 = 0xE0
FILL_VAL = b'\xFF'
END_CHAR = '\x00'

# OUT = """
# Name der Image Datei:            {name}
# Bootsektor Informationen:
 # Version:                        FAT{ver}
 # OEM Name:                       {oem}
 # Sektorgroesse:                  {se_size} Byte
 # Clustergroesse:                 {cl_size} Byte
 # Groesse des Wurzel Verzeichnis: {size_root} Sektoren
 # Erster Datensektor:             {first_se}
 # Anzahl FAT Kopien:              {cop}
 # Groesse der FAT:                {size_fat} Sektoren
 # Erster Fat Sektor               {fist_fat_se}
 # Anzahl der Sektoren:            {se_count}
 # Anzahl der Cluster:             {cl_count}
 # Gesamtsektoranzahl 16 Bit:      {count16}
 # Gesamtsektoranzahl 32 Bit:      {count32}
 # Media Descriptor Byte:          0x{mdb_hx:X}
 # {mdb}
 # Sektoren pro Spur:              {se_track}
 # Anzahl der Seiten
 # bzw. Schreib-Lese-Koepfe:       {heads_sides}
 # Anzahl der (versteckten)
 # Sektoren vor dem Bootsektor:    {hid_se}
# """

OUT = """
Image file name:                {name}
Boot sector information:
 Version:                       FAT{ver}
 OEM name:                      {oem}
 Bytes per sector:              {se_size} Byte
 Sectors per cluster:           {cl_size} Byte
 Size of root dir:              {size_root} sectors
 First data sector:             {first_se}
 Number of FAT's:               {cop}
 Size of FAT:                   {size_fat} Sektoren
 First FAT sector:              {fist_fat_se}
 Number of sectors:             {se_count}
 Number of cluster:             {cl_count}
 Total logical volume sectors:  {count16}
 Large amount of sector:        {count32}
 Media descriptor Byte:         0x{mdb_hx:X}
 {mdb}
 Number of sectors per track:   {se_track}
 Number of heads or sides:      {heads_sides}
 Number of hidden sectors:      {hid_se}
"""
# OUT16 = """
# FAT12 und FAT16 Informationen:
 # Physische BIOS-LaufwerksNr:     {dr_nr} 0x{dr_nr:X}
 # Reserviert:                     {rsv}
 # Erweiterte Bootsignatur:        {sign}
 # Dateisystem-ID (Seriennummer):  {vol_id}
 # Name des Dateisystems:          {vol_lbl}
 # FAT-Variante:                   {fat_vari}
 # BIOS-Bootsektorsignatur:        {boot_sig} 0x{boot_sig:X}

# """
OUT16 = """
FAT12 and FAT16 information:
 Drive number:                  {dr_nr} 0x{dr_nr:X}
 Reserved:                      {rsv}
 Boot signature:                {sign}
 VolumeID 'Serial' number:      {vol_id}
 Volume label:                  {vol_lbl}
 System identifier:             {fat_vari}
 Bootable partition signature:  {boot_sig} 0x{boot_sig:X}

"""
# OUT32 = """
# Fat32 Informationen:
 # Anzahl der Sektoren pro FAT:    {sek_p_fat}
 # FAT Flags:                      {fat_flags}
 # FAT-32-Version:                 {fat32_ver}
 # Beginn Stammverzeichnis:        {root_cluster_nr}
 # FS Information Sektor:          {sekt_nr_fs}
 # Bootsektorkopie Sektor:         {sekt_nr_bo}
 # reserviert:                     {rsv_e}
 # Physische BIOS-Laufwerksnummer: {p_bios_hw_nr}
 # beschaedigtes Dateisystem:      {rsv_b}
 # Erweiterte Bootsignatur:        {ext_boot_sig}
 # Dateisystem-ID (Seriennummer):  {datsys_id}
 # Name des Dateisystems:          {fs_name}
 # FAT-Version:                    {fat_ver}
 # BIOS-Bootsektorsignatur:        {bios_bootsek_sig}

# """
OUT32 = """
FAT32 informationen:
 Sectors per FAT:               {sek_p_fat}
 Flags:                         {fat_flags}
 FAT32 version:                 {fat32_ver}
 Root directory cluster number: {root_cluster_nr}
 FSInfo sector number:          {sekt_nr_fs}
 Backup boot sector number:     {sekt_nr_bo}
 Reserved:                      {rsv_e}
 Drive number:                  {p_bios_hw_nr}
 Windows NT Flags (Reserved):   {rsv_b}
 Signature:                     {ext_boot_sig}
 VolumeID 'Serial' number:      {datsys_id}
 File system name:              {fs_name}
 FAT version:                   {fat_ver}
 Bootable partition signature:  {bios_bootsek_sig}

"""

# MD_ERR = ' Media Descriptor kann nicht gelesen werden'
# https://de.wikipedia.org/wiki/File_Allocation_Table#Bootsektor
# MEDIA_DSCR_INFO = {
    # 0xF0: '  Double sided, 80 Spuren, 18/36 Sektoren pro Spur (1440 KiB sowie 2880 KiB 3.5-Diskette)',
    # 0xF8: '  Festplatte',
    # 0xF9: '  Double sided, 80 Spuren, 9/15 Sektoren pro Spur (720 KiB 3.5- bzw. 1200 KiB-5.25-Diskette)',
    # 0xFA: '  Single sided, 80 Spuren, 8 Sektoren pro Spur(320 KiB 3.5- bzw. 5.25-Diskette). Wird auch für Ramdisks verwendet',
    # 0xFB: '  Double sided, 80 Spuren, 8 Sektoren pro Spur (640 KiB 3.5- bzw. 5.25-Diskette)',
    # 0xFC: '  Single sided, 40 Spuren, 9 Sektoren pro Spur (180 KiB 5.25-Diskette)',
    # 0xFD: '  Double sided, 40 Spuren, 9 Sektoren pro Spur (360 KiB 5.25-Diskette)  Double Sided, 77 Spuren, 26 Sektoren pro Spur (500 KiB 8-Disketten)',
    # 0xFE: '  Double sided, 40 Spuren, 9 Sektoren pro Spur (360 KiB 5.25-Diskette)  Double Sided, 77 Spuren, 26 Sektoren pro Spur (500 KiB 8-Disketten)  Single sided, 40 Spuren, 8 Sektoren pro Spur (160 KiB-5.25-Diskette)  Single Sided, 77 Spuren, 26 Sektoren pro Spur (250 KiB 8-Disketten)  Double Sided, 77 Spuren, 8 Sektoren pro Spur (1200 KiB 8-Disketten)',
    # 0xFF: '  Double sided, 40 Spuren, 8 Sektoren pro Spur (320 KiB 5.25-Diskette)'}

MD_ERR = ' can not read media descriptor'
# https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system#media
MEDIA_DSCR_INFO = {
    0xE5: ' 8-inch (200 mm) Single sided, 77 tracks per side, 26 sectors per track, 128 bytes per sector (250.25 KB) (DR-DOS only)',
    0xED: ' 5.25-inch (130 mm) Double sided, 80 tracks per side, 9 sector, 720 KB (Tandy 2000 only)[18]',
    0xEE: ' Designated for non-standard custom partitions (utilizing non-standard BPB formats or requiring special media access such as 48-/64-bit addressing); corresponds with 0xF8, but not recognized by unaware systems by design; value not required to be identical to FAT ID, never used as cluster end-of-chain marker (Reserved for DR-DOS)',
    0xEF: ' Designated for non-standard custom superfloppy formats; corresponds with 0xF0, but not recognized by unaware systems by design; value not required to be identical to FAT ID, never used as cluster end-of-chain marker (Reserved for DR-DOS)',
    0xF0: ' 3.5-inch (90 mm) Double Sided, 80 tracks per side, 18 or 36 sectors per track (1440 KB, known as “1.44 MB”; or 2880 KB, known as “2.88 MB”)\n  Designated for use with custom floppy and superfloppy formats where the geometry is defined in the BPB.\n  Used also for other media types such as tapes.[29]',
    0xF4: ' Double density (Altos MS-DOS 2.11 only)',
    0xF5: ' Fixed disk, 4-sided, 12 sectors per track (1.95? MB) (Altos MS-DOS 2.11 only)',
    0xF8: ' Fixed disk (i.e., typically a partition on a hard disk). (since DOS 2.0)\n  Designated to be used for any partitioned fixed or removable media, where the geometry is defined in the BPB.\n  3.5-inch Single sided, 80 tracks per side, 9 sectors per track (360 KB) (MS-DOS 3.1[12] and MSX-DOS)\n  5.25-inch Double sided, 80 tracks per side, 9 sectors per track (720 KB) (Sanyo 55x DS-DOS 2.11 only)\n  Single sided (Altos MS-DOS 2.11 only)',
    0xF9: ' 3.5-inch Double sided, 80 tracks per side, 9 sectors per track (720 KB) (since DOS 3.2)\n  3.5-inch Double sided, 80 tracks per side, 18 sectors per track (1440 KB) (DOS 3.2 only)\n  5.25-inch Double sided, 80 tracks per side, 15 sectors per track (1200 KB, known as “1.2 MB”) (since DOS 3.0)\n  Single sided (Altos MS-DOS 2.11 only)',
    0xFA: ' 3.5-inch and 5.25-inch Single sided, 80 tracks per side, 8 sectors per track (320 KB)\n  Used also for RAM disks and ROM disks (e.g., on Columbia Data Products and on HP 200LX)\n  Hard disk (Tandy MS-DOS only)',
    0xFB: ' 3.5-inch and 5.25-inch Double sided, 80 tracks per side, 8 sectors per track (640 KB)',
    0xFC: ' 5.25-inch Single sided, 40 tracks per side, 9 sectors per track (180 KB) (since DOS 2.0)',
    0xFD: ' 5.25-inch Double sided, 40 tracks per side, 9 sectors per track (360 KB) (since DOS 2.0)\n  8-inch Double sided, 77 tracks per side, 26 sectors per track, 128 bytes per sector (500.5 KB)\n  (8-inch Double sided, (single and) double density (DOS 1))',
    0xFE: ' 5.25-inch Single sided, 40 tracks per side, 8 sectors per track (160 KB) (since DOS 1.0)\n  8-inch Single sided, 77 tracks per side, 26 sectors per track, 128 bytes per sector (250.25 KB)\n  8-inch Double sided, 77 tracks per side, 8 sectors per track, 1024 bytes per sector (1232 KB)\n  (8-inch Single sided, (single and) double density (DOS 1))',
    0xFF: ' 5.25-inch Double sided, 40 tracks per side, 8 sectors per track (320 KB) (since DOS 1.1)\n  Hard disk (Sanyo 55x DS-DOS 2.11 only)',}


# FAT_TABLE = 'Die FAT als Tabelle in Hexadezimal:\n'
FAT_TABLE = 'FAT as table:\n'

# file attributes
READ_ONLY = 0x01
HIDDEN = 0x02
SYSTEM = 0x04
VOLUME_LABLE = 0x08
SUB_DIR = 0x10
ACHIVE = 0x20
DEVICE = 0x40
RSV_ATRB = 0x80

# directories
FILE_NAME_8_3 = 0x00
EXT_NAME = 0x08
FILE_ATRB = 0x0B
RSV_DIR = 0x0C
CREATE_TIME = 0x0E
CREATE_DATE = 0x10
LAST_ACCESS_DATE = 0x12
FAT32_16BITS_CLUSTER = 0x14
LAST_CHANGE_TIME = 0x16
LAST_CHANGE_DATE = 0x18
START_CLUSTER = 0x1A
FILE_SIZE = 0x1C

# FILES_DIRS =  'Dateien und Vezeichnisse:\n'
FILES_DIRS =  'Files and directories:\n'

FS_NAME = '{0}\\{1}{2}{3}'

# FS_DATE_TIME = 'am {2}.{1}.{0} um {3}:{4}:{5}'
FS_DATE_TIME = '{0}-{1}-{2} {3:02d}:{4:02d}:{5:02d}'
# FS_DATE = 'am {2}.{1}.{0}'
FS_DATE = 'at {0}-{1}-{2}'

# OUT_DIR = """Name:               {name}
# Longe File Name:    {l_f_name}
# Geloescht:          {del}
# Dot entry:          {dot_ent}
# VFAT Eintrag:       {vfat}
# Schreibgeschuetzt:  {ro}
# Versteckt:          {hid}
# Systemdatei:        {sys_f}
# Volume-Label:       {vol_lbl}
# Unterverzeichnis:   {sub_dir}
# Archiv:             {achive}
# Geraetedatei:       {device_f}
# Reserviert:         {rsv}
# Erstellt            {create}
# Letzter Zugriff     {last_access}
# Letzte Aenderung    {last_chn}
# FAT Start Cluster:  {start_cluster} 0x{start_cluster:X}
# Dateigroesse:       {f_size} Byte
# """
OUT_DIR = """Name 8.3:           {name}
Longe file name:    {l_f_name}
Deleted:            {del}
Dot entry:          {dot_ent}
VFAT entry:         {vfat}
READ ONLY:          {ro}
HIDDEN:             {hid}
SYSTEM:             {sys_f}
VOLUME ID:          {vol_lbl}
DIRECTORY:          {sub_dir}
ARCHIVE:            {achive}
DEVICE:             {device_f}
Reserved:           {rsv}
Created             {create}
Last accesse        {last_access}
Letzte Aenderung    {last_chn}
FAT start cluster:  {start_cluster}
File size:          {f_size} Byte
"""

# FILES_SAVE = 'Dateien werden Gespeichert:\n'
FILES_SAVE = 'Save files:\n'
# FILE_SAVED = ' wurde gespeichert\n'
FILE_SAVED = ' saved\n'

if __name__ == '__main__':
    pass
    # print(MEDIA_DSCR_INFO[0xFE])
