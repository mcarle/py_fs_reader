#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import struct
from collections import namedtuple
import sys
import time
import os
import os.path

if __name__ != '__main__':
    from fs_libs.helper import fs_helper as fs_h

class Fat(object):
    """
    read fat image file and print information, can also save files from image
    """
    def __init__(self, datei_name, log_file_name=''):
        self.fat_lst = []  # fat as list
        self.file_list = []  # all file / folders as list
        self.offset = 0
        self.bytes_per_sect = fs_h.SECT_SIZE  # 512 byte
        self.ver = 0  # 12, 16 or 32
        self.root_dir_sectors = 0 # root directory size(if FAT32 == 0)
        self.first_data_sector = 0 # first data sector (first sector for files / dirs)
        self.first_fat_sector = 0 # first sector in fat
        self.data_sectors = 0 # total number of data sectors
        self.total_clusters = 0 # total number of clusters
        self.cluster_size = 0

        self.table_width = 16
        self.table_hex_width = 0  # hex width in table for print and log

        self.fat_free_cluster = 0
        self.fat_rsv_intern = 0
        self.fat_rsv_do_not_use = 0
        self.fat_bad_sektor = 0

        try:
            self.img_file = open(datei_name, 'rb')
        except:
            print(fs_h.IMG_FILE_ERR)
            sys.exit(1)

        if log_file_name:
            try:
                self.log_file = open(log_file_name, 'w', encoding=fs_h.ENC_UTF8)
            except:
                print(fs_h.LOG_FILE_ERR)
                sys.exit(1)
        else:
            self.log_file = None

    @staticmethod
    def cts(x, a, b, c): # cluster nr. to sector
        return (a + b) + (x - 2) * c

    @staticmethod
    def get_sekt_nr(y, x):  # jump for sector x
        return y * x

    def start(self, offset):
        self.offset = offset
        self.img_file.seek(self.offset * self.bytes_per_sect)  # partion offset (if 0, partion is image)
        boot_sector = self.img_file.read(self.bytes_per_sect)
        self.__read_bootsektor(boot_sector)
        # if fat32, root_dir_sectors == 0
        self.root_dir_sectors = ((self.dir_entries * fs_h.BYTE_SIZE_FAT_DIR) + (self.bytes_per_sect - 1)) // self.bytes_per_sect
        self.first_data_sector = self.rsv_sects + (self.fat_copies * self.sects_per_fat)
        self.first_fat_sector = self.rsv_sects
        self.data_sectors = (self.total_sects + self.large_amount_sects) - (self.rsv_sects + (self.fat_copies * self.sects_per_fat) + self.root_dir_sectors)
        self.total_clusters = self.data_sectors // self.sect_per_clust
        self.cluster_size = self.bytes_per_sect * self.sect_per_clust

        if self.total_clusters < fs_h.MAX_CLUSTER_12:
            self.table_hex_width = fs_h.HEX_DIGITS_12
            self.ver = fs_h.FAT12
            self.__read_bootsektor_12_16(boot_sector)  # only fat12/16
            self.fat_free_cluster = fs_h.FREE_CLUSTER_12
            self.fat_rsv_intern = fs_h.RSV_INTERN_12
            self.fat_rsv_do_not_use = fs_h.RSV_DO_NOT_USE_12
            self.fat_bad_sektor = fs_h.BAD_SEKTOR_12
        else:
            if self.total_clusters < fs_h.MAX_CLUSTER_16:
                self.table_hex_width = fs_h.HEX_DIGITS_16
                self.ver = fs_h.FAT16
                self.__read_bootsektor_12_16(boot_sector)  # only fat12/16
                self.fat_free_cluster = fs_h.FREE_CLUSTER_16
                self.fat_rsv_intern = fs_h.RSV_INTERN_16
                self.fat_rsv_do_not_use = fs_h.RSV_DO_NOT_USE_16
                self.fat_bad_sektor = fs_h.BAD_SEKTOR_16
            else:
                self.table_hex_width = fs_h.HEX_DIGITS_32
                self.ver = fs_h.FAT32
                self.__read_bootsektor_32(boot_sector)  # only fat32
                self.fat_free_cluster = fs_h.FREE_CLUSTER_32
                self.fat_rsv_intern = fs_h.RSV_INTERN_32
                self.fat_rsv_do_not_use = fs_h.RSV_DO_NOT_USE_32
                self.fat_bad_sektor = fs_h.BAD_SEKTOR_32

    #type of cluster per fat version
    @staticmethod
    def fat_data_cluster(x, vers):
        if vers == fs_h.FAT12:
            return True if fs_h.DATA_CLUSTER_START_12 <= x <= fs_h.DATA_CLUSTER_END_12 else False
        elif vers == fs_h.FAT16:
            return True if fs_h.DATA_CLUSTER_START_16 <= x <= fs_h.DATA_CLUSTER_END_16 else False
        elif vers == fs_h.FAT32:
            return True if fs_h.DATA_CLUSTER_START_32 <= (x & fs_h.FAT32_28_BIT) <= fs_h.DATA_CLUSTER_END_32 else False

    @staticmethod
    def fat_rsv_context(x, vers):
        if vers == fs_h.FAT12:
            return True if fs_h.RSV_CONTEXT_START_12 <= x <= fs_h.RSV_CONTEXT_END_12 else False
        elif vers == fs_h.FAT16:
            return True if fs_h.RSV_CONTEXT_START_16 <= x <= fs_h.RSV_CONTEXT_END_16 else False
        elif vers == fs_h.FAT32:
            return True if fs_h.RSV_CONTEXT_START_32 <= x <= fs_h.RSV_CONTEXT_END_32 else False

    @staticmethod
    def fat_last_cluster(x, vers):
        if vers == fs_h.FAT12:
            return True if fs_h.LAST_CLUSTER_START_12 <= x <= fs_h.LAST_CLUSTER_END_12 else False
        elif vers == fs_h.FAT16:
            return True if fs_h.LAST_CLUSTER_START_16 <= x <= fs_h.LAST_CLUSTER_END_16 else False
        elif vers == fs_h.FAT32:
            return True if fs_h.LAST_CLUSTER_START_32 <= (x & fs_h.FAT32_28_BIT) <= fs_h.LAST_CLUSTER_END_32 else False

    def __read_bootsektor(self, boot_sector):
        '''
        read bootsector without fat 12/16/32 parts
        '''
        self.machine_code = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(3), boot_sector, fs_h.MACHINE_CODE_OFFSET)[0]
        self.oem_name = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(8), boot_sector, fs_h.OEM_NAME_OFFSET)[0]
        self.bytes_per_sect = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.BYTES_PER_SECT_OFFSET)[0]
        self.sect_per_clust = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.SECT_PER_CLUST_OFFSET)[0]
        self.rsv_sects = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.RSV_SECTS_OFFSET)[0]
        self.fat_copies = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.FAT_COPIES_OFFSET)[0]
        self.dir_entries = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.DIR_ENTRIES_OFFSET)[0]
        self.total_sects = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.TOTAL_SECTS_OFFSET)[0]
        self.media_diskrp_byte = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.MEDIA_DISKRP_BYTE_OFFSET)[0]
        self.sects_per_fat = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.SECTS_PER_FAT_OFFSET)[0]
        self.fat_size = self.sects_per_fat * self.bytes_per_sect
        self.sect_per_track = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.SECT_PER_TRACK_OFFSET)[0]
        self.nr_sides = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.NR_SIDES_OFFSET)[0]
        self.hidden_sects = struct.unpack_from(fs_h.U_INT_LIT, boot_sector, fs_h.HIDDEN_SECTS_OFFSET)[0]
        self.large_amount_sects = struct.unpack_from(fs_h.U_INT_LIT, boot_sector, fs_h.LARGE_AMOUNT_SECTS_OFFSET)[0]

    def __read_bootsektor_12_16(self, boot_sector):
        '''
        read bootsector fat12/16 parts
        '''
        self.phy_drive_nr = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.PHY_DRIVE_NR_OFFSET)[0]
        self.rsv_fat12_16 = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.RSV_FAT12_16_OFFSET)[0]
        self.ext_boot_sig = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.EXT_BOOT_SIG_OFFSET)[0]
        self.volm_id = struct.unpack_from(fs_h.U_INT_LIT, boot_sector, fs_h.VOLM_ID_OFFSET)[0]
        self.part_volm_lable = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(11), boot_sector, fs_h.PART_VOLM_LABLE_OFFSET)[0]
        self.fat_variante = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(8), boot_sector, fs_h.FAT_VARIANTE_OFFSET)[0]
        self.boot_code = boot_sector[fs_h.BOOT_CODE_OFFSET_START:fs_h.BOOT_CODE_OFFSET_END]
        self.bios_boot_sig = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.BIOS_BOOT_SIG_OFFSET)[0]

    def __read_bootsektor_32(self, boot_sector):
        '''
        read bootsector fat32 parts
        '''
        self.sect_per_fat = struct.unpack_from(fs_h.U_INT_LIT, boot_sector, fs_h.SECT_PER_FAT_OFFSET)[0]
        self.first_data_sector = self.rsv_sects + (self.fat_copies * self.sect_per_fat)
        self.fat_size = self.sect_per_fat * self.bytes_per_sect
        self.fat_flags = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.FAT_FLAGS_OFFSET)[0]
        self.fat32_ver = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.FAT32_VER_OFFSET)[0]
        self.root_cluster_nr = struct.unpack_from(fs_h.U_INT_LIT, boot_sector, fs_h.ROOT_CLUSTER_NR_OFFSET)[0]
        self.fsi_sect = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.FSI_SECT_OFFSET)[0]
        self.sect_nr_boot_sect_copy = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.SECT_NR_BOOT_SECT_COPY_OFFSET)[0]
        self.rsv_e = boot_sector[fs_h.RSV_E_OFFSET_START:fs_h.RSV_E_OFFSET_END]
        self.drive_nr = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.DRIVE_NR_OFFSET)[0]
        self.rsv_b = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.RSV_B_OFFSET)[0]
        self.ext_boot_sig = struct.unpack_from(fs_h.U_CHAR_LIT, boot_sector, fs_h.EXT_BOOT_SIG_OFFSET)[0]
        self.fs_id = struct.unpack_from(fs_h.U_INT_LIT, boot_sector, fs_h.FS_ID_OFFSET)[0]
        self.fs_name = boot_sector[fs_h.FS_NAME_OFFSET_START:fs_h.FS_NAME_OFFSET_END]
        self.fat_ver = boot_sector[fs_h.FAT_VER_OFFSET_START:fs_h.FAT_VER_OFFSET_END]
        self.boot_code = boot_sector[fs_h.BOOT_CODE_32_OFFSET_START:fs_h.BOOT_CODE_32_OFFSET_END]
        self.bios_bootsek_sig = struct.unpack_from(fs_h.U_SHORT_LIT, boot_sector, fs_h.BIOS_BOOTSEK_SIG_OFFSET)[0]

    def close(self):
        self.img_file.close()
        if self.log_file:
            self.log_file.close()

    def __print_to(self, out_lst, display):
        '''
        print information on screen or in log file
        '''
        out_str = ''.join(out_lst)
        if display:
            print(out_str, end='\n')
        if self.log_file:
            self.log_file.write(out_str)

    def print_bootsektor_info(self, display=True):
        tmp_fmt = {}
        tmp_fmt['name'] = self.img_file.name
        tmp_fmt['ver'] = self.ver
        tmp_fmt['oem'] = self.oem_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS)
        tmp_fmt['se_size'] = self.bytes_per_sect
        tmp_fmt['cl_size'] = self.cluster_size
        tmp_fmt['size_root'] = self.root_dir_sectors
        tmp_fmt['first_se'] = self.first_data_sector
        tmp_fmt['cop'] = self.fat_copies
        tmp_fmt['size_fat'] = self.sects_per_fat
        tmp_fmt['fist_fat_se'] = self.first_fat_sector
        tmp_fmt['se_count'] = self.data_sectors
        tmp_fmt['cl_count'] = self.total_clusters
        tmp_fmt['count16'] = self.total_sects
        tmp_fmt['count32'] = self.large_amount_sects
        tmp_fmt['mdb_hx'] = self.media_diskrp_byte
        tmp_fmt['mdb'] = fs_h.MEDIA_DSCR_INFO.get(self.media_diskrp_byte, fs_h.MD_ERR)
        tmp_fmt['se_track'] = self.sect_per_track
        tmp_fmt['heads_sides'] = self.nr_sides
        tmp_fmt['hid_se'] = self.hidden_sects
        ausgabe = [fs_h.OUT.format(**tmp_fmt)]

        if self.ver <= fs_h.FAT16:
            tmp_fmt16 = {}
            tmp_fmt16['dr_nr'] = self.phy_drive_nr
            tmp_fmt16['rsv'] = self.rsv_fat12_16
            tmp_fmt16['sign'] = self.ext_boot_sig
            tmp_fmt16['vol_id'] = self.volm_id
            tmp_fmt16['vol_lbl'] = self.part_volm_lable
            tmp_fmt16['fat_vari'] = self.fat_variante
            tmp_fmt16['boot_sig'] = self.bios_boot_sig
            ausgabe.extend(fs_h.OUT16.format(**tmp_fmt16))

        elif self.ver == fs_h.FAT32:
            tmp_fmt32 = {}
            tmp_fmt32['sek_p_fat'] = self.sect_per_fat
            tmp_fmt32['fat_flags'] = self.fat_flags
            tmp_fmt32['fat32_ver'] = self.fat32_ver
            tmp_fmt32['root_cluster_nr'] = self.root_cluster_nr
            tmp_fmt32['sekt_nr_fs'] = self.fsi_sect
            tmp_fmt32['sekt_nr_bo'] = self.sect_nr_boot_sect_copy
            # tmp_fmt32['rsv_e'] = self.rsv_e.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS)
            tmp_fmt32['rsv_e'] = 0
            tmp_fmt32['p_bios_hw_nr'] = self.drive_nr
            tmp_fmt32['rsv_b'] = self.rsv_b
            tmp_fmt32['ext_boot_sig'] = self.ext_boot_sig
            tmp_fmt32['datsys_id'] = self.fs_id
            tmp_fmt32['fs_name'] = self.fs_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS)
            tmp_fmt32['fat_ver'] = self.fat_ver.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS)
            tmp_fmt32['bios_bootsek_sig'] = self.bios_bootsek_sig
            ausgabe.extend(fs_h.OUT32.format(**tmp_fmt32))

        self.__print_to(ausgabe, display)

    def __read_sektor(self, start, size):
        """
        read one ore more sectors
        """
        self.img_file.seek(self.offset * self.bytes_per_sect)
        self.img_file.seek(self.get_sekt_nr(self.bytes_per_sect, start), 1)
        return self.img_file.read(size)

    def get_files(self, display=True):
        """
        read root dirs
        """
        if self.ver <= fs_h.FAT16:
            self.__read_dir(self.__read_sektor(self.first_data_sector, self.root_dir_sectors * self.bytes_per_sect), display, [])
        elif self.ver == fs_h.FAT32:
            # self.__read_dir(self.__read_sektor(self.first_data_sector, self.cluster_size), display, [])
            for s in self.__fat_to_cluster(self.root_cluster_nr):
                sub_dir_cluster = self.__read_sektor(self.cts(s, self.first_data_sector, self.root_dir_sectors, self.sect_per_clust), self.cluster_size)
                self.__read_dir(sub_dir_cluster, display, [])

    def __fat_to_cluster(self, cluster_nr):
        """
        read cluste numbers from fat
        """
        nr = cluster_nr
        if self.ver == fs_h.FAT32:  # fat32 number size 28 bit
            nr = (nr & fs_h.FAT32_28_BIT)
        yield nr
        while True:
            nr = self.fat_lst[nr]
            if not self.fat_data_cluster(nr, self.ver):
                break
            elif self.fat_last_cluster(nr, self.ver):
                break
            yield nr

    def __fat_to_list(self, f_table):
        """
        make a list from fat
        """
        if self.ver == fs_h.FAT12:
            for i in range(0, len(f_table), fs_h.FAT12_SIZE):
                if i + fs_h.FAT12_SIZE > len(f_table):
                    break

                fat12_3_byte = struct.unpack(fs_h.CHAR_ARRAY_LIT(3), f_table[i:i + fs_h.FAT12_SIZE])[0]
                first_12_bit = int.from_bytes(fat12_3_byte, byteorder='little') & fs_h.HALF_24_BIT_LAST
                last_12_bit = (int.from_bytes(fat12_3_byte, byteorder='little') & fs_h.HALF_24_BIT_FIRST) >> fs_h.RIGHT_SHIFT_12_BIT
                self.fat_lst.append(first_12_bit)
                self.fat_lst.append(last_12_bit)

        elif self.ver == fs_h.FAT16:
            for i in range(0, len(f_table), fs_h.FAT16_SIZE):
                self.fat_lst.append(struct.unpack(fs_h.U_SHORT_LIT, f_table[i:i + fs_h.FAT16_SIZE])[0])
        elif self.ver == fs_h.FAT32:
            for i in range(0, len(f_table), fs_h.FAT32_SIZE):
                self.fat_lst.append(struct.unpack(fs_h.U_LONG_LIT, f_table[i:i + fs_h.FAT32_SIZE])[0])

    def read_print_fat(self, display=True):
        """
        read first fat and if needed print as hex table
        """
        self.__fat_to_list(self.__read_sektor(self.first_fat_sector, self.fat_size))

        if not display:
            return

        i = 0
        table = [fs_h.FAT_TABLE]
        space = ''.join([' '] * self.table_hex_width)
        line = ''.join(['-'] * ((self.table_width * self.table_hex_width) + self.table_width-1))
        fmt_str = f'{{:0{self.table_hex_width}X}}'
        first_line = [fmt_str.format(x) for x in range(self.table_width)]
        table.extend([space, ' |', '|'.join(first_line), '|', '\n'])
        table.extend([space, ' |', line, '|', '\n'])
        for f in range(0, len(self.fat_lst), self.table_width):
            tmp = self.fat_lst[f:self.table_width + f]
            tmp = [fmt_str.format(x) for x in tmp]
            table.extend([fmt_str.format(i), ' |', '|'.join(tmp), '|', '\n'])
            i += self.table_width

        table.append('\n\n')
        self.__print_to(table, display)

    @staticmethod
    def __read_date_time(r_date, r_time):
        """
        return date and time for create, access, change
        """
        year = (r_date >> 9) + 1980
        month = (r_date >> 5) & 0xF
        day = (r_date & 0x001F)

        hour = (r_time >> 11)
        minute = (r_time >> 5) & 0x3F
        second = (r_time & 0x001F) * 2

        return (year, month, day), (hour, minute, second)

    @staticmethod
    def __read_file_attributes(atrib):
        """
        read and return file attributes
        """
        atr_lst = ['read_only',
                   'hidden',
                   'system',
                   'volume_lable',
                   'sub_dir',
                   'achive',
                   'device',
                   'rsv']
        file_atribs = namedtuple('File_atribs', atr_lst)

        atr_dic = {}
        atr_dic['read_only'] = atrib & fs_h.READ_ONLY
        atr_dic['hidden'] = atrib & fs_h.HIDDEN
        atr_dic['system'] = atrib & fs_h.SYSTEM
        atr_dic['volume_lable'] = atrib & fs_h.VOLUME_LABLE
        atr_dic['sub_dir'] = atrib & fs_h.SUB_DIR
        atr_dic['achive'] = atrib & fs_h.ACHIVE
        atr_dic['device'] = atrib & fs_h.DEVICE
        atr_dic['rsv'] = atrib & fs_h.RSV_ATRB

        return file_atribs(**atr_dic)

    def __read_dir(self, root_dir, display, dir_name):
        """
        first read root dir and other dirs
        """
        size = fs_h.BYTE_SIZE_FAT_DIR  # size dir entry
        tmp_lst = []
        long_file_name_lst = []

        self.__print_to([fs_h.FILES_DIRS], display)

        for i in range(0, len(root_dir), size):
            dir_file = root_dir[i:i + size]

            if dir_file[0] == 0:
                continue

            file_name = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(8), dir_file, fs_h.FILE_NAME_8_3)[0]
            ext_name = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(3), dir_file, fs_h.EXT_NAME)[0]
            file_atrb = struct.unpack_from(fs_h.U_CHAR_LIT, dir_file, fs_h.FILE_ATRB)[0]
            # rsv = struct.unpack_from(fs_h.U_CHAR_LIT, dir_file, fs_h.RSV_DIR)[0]
            create_time = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.CREATE_TIME)[0]
            create_date = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.CREATE_DATE)[0]
            last_access_date = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.LAST_ACCESS_DATE)[0]
            # fat32_16bits_cluster = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.FAT32_16BITS_CLUSTER)[0]

            last_change_time = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.LAST_CHANGE_TIME)[0]
            last_change_date = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.LAST_CHANGE_DATE)[0]
            start_cluster = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.START_CLUSTER)[0]
            file_size = struct.unpack_from(fs_h.U_SHORT_LIT, dir_file, fs_h.FILE_SIZE)[0]

            # convert date and time
            fa = self.__read_file_attributes(file_atrb)
            cd, ct = self.__read_date_time(create_date, create_time)
            lad = self.__read_date_time(last_access_date, 0)
            lad = lad[0]
            lcd, lct = self.__read_date_time(last_change_date, last_change_time)

            test_file_name = file_name[0] != fs_h.DELETED and file_name[0] != fs_h.DOT_ENTRY
            test_atrib = not fa.system and not fa.volume_lable and not fa.device and not fa.rsv

            test_vfat = test_file_name and fa.read_only and fa.hidden and fa.system and fa.volume_lable
            test_subdir = test_file_name and test_atrib and fa.sub_dir
            test_file = test_file_name and test_atrib and not fa.sub_dir

            # make longe file name (todo checksum)
            if test_vfat:
                vfat_data = self.__vfat(dir_file)
                # first entry is end, last ist start
                if vfat_data.nr >= 2 and vfat_data.first_pyhs_entry:
                    tmp_lst.append(vfat_data.long_file_name)
                elif vfat_data.nr == 1:
                    tmp_lst.append(vfat_data.long_file_name)
                    tmp_lst.reverse()
                    long_file_name_lst.append(''.join(tmp_lst))
                    tmp_lst = []

                long_file_name = ''
            else:
                if long_file_name_lst:
                    long_file_name = long_file_name_lst.pop(0)
                else:
                    long_file_name = ''

            info_lst = ['dir_name',
                        'file_name',
                        'ext_name',
                        'long_file_name',
                        'vfat',
                        'fa',
                        'cd',
                        'ct',
                        'lad',
                        'lcd',
                        'lct',
                        'start_cluster',
                        'file_size']

            info = namedtuple('Infos', info_lst)
            info_dict = {}
            info_dict['dir_name'] = dir_name[:]
            info_dict['file_name'] = file_name
            info_dict['ext_name'] = ext_name
            info_dict['long_file_name'] = long_file_name
            info_dict['vfat'] = test_vfat
            info_dict['fa'] = fa
            info_dict['cd'] = cd
            info_dict['ct'] = ct
            info_dict['lad'] = lad
            info_dict['lcd'] = lcd
            info_dict['lct'] = lct
            info_dict['start_cluster'] = start_cluster
            info_dict['file_size'] = file_size
            infos = info(**info_dict)

            if display or self.log_file:
                self.__print_dir(infos, display)

            # if file save info in list
            if test_file:
                self.file_list.append(infos)

            # scan subdir recursive
            if test_subdir:
                if long_file_name:
                    dir_name.append(long_file_name)
                else:
                    dir_name.append(file_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS).strip())

                for s in self.__fat_to_cluster(start_cluster):
                    sub_dir_cluster = self.__read_sektor(
                        self.cts(s, self.first_data_sector, self.root_dir_sectors, self.sect_per_clust), self.cluster_size)
                    self.__read_dir(sub_dir_cluster, display, dir_name[:])
                dir_name = []

    def __print_dir(self, infos, display):
        """
        output for files and dirs
        """
        dir_name = '\\'.join(infos.dir_name)
        name_format = (dir_name,
                       infos.file_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS).strip(),
                       '' if infos.fa.sub_dir else '.',
                       infos.ext_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS).strip())
        # YYYY-MM-DD und hh:mm:ss
        create_format = (infos.cd[0], infos.cd[1], infos.cd[2], infos.ct[0], infos.ct[1], infos.ct[2])
        # YYYY-MM-DD und hh:mm:ss
        last_chn_format = (infos.lcd[0], infos.lcd[1], infos.lcd[2], infos.lct[0], infos.lct[1], infos.lct[2])
        # YYYY-MM-DD
        last_access = (infos.lad[0], infos.lad[1], infos.lad[2])

        tmp_fmt_dr = {}
        tmp_fmt_dr['name'] = fs_h.FS_NAME.format(*name_format)
        tmp_fmt_dr['l_f_name'] = infos.long_file_name if infos.long_file_name else fs_h.NO
        tmp_fmt_dr['del'] = fs_h.YES if infos.file_name[0] == fs_h.DELETED else fs_h.NO
        tmp_fmt_dr['dot_ent'] = fs_h.YES if infos.file_name[0] == fs_h.DOT_ENTRY else fs_h.NO
        tmp_fmt_dr['vfat'] = fs_h.YES if infos.vfat else fs_h.NO
        tmp_fmt_dr['ro'] = fs_h.YES if infos.fa.read_only else fs_h.NO
        tmp_fmt_dr['hid'] = fs_h.YES if infos.fa.hidden else fs_h.NO
        tmp_fmt_dr['sys_f'] = fs_h.YES if infos.fa.system else fs_h.NO
        tmp_fmt_dr['vol_lbl'] = fs_h.YES if infos.fa.volume_lable else fs_h.NO
        tmp_fmt_dr['sub_dir'] = fs_h.YES if infos.fa.sub_dir else fs_h.NO
        tmp_fmt_dr['achive'] = fs_h.YES if infos.fa.achive else fs_h.NO
        tmp_fmt_dr['device_f'] = fs_h.YES if infos.fa.device else fs_h.NO
        tmp_fmt_dr['rsv'] = fs_h.YES if infos.fa.rsv else fs_h.NO
        tmp_fmt_dr['create'] = fs_h.FS_DATE_TIME.format(*create_format)
        tmp_fmt_dr['last_access'] = fs_h.FS_DATE.format(*last_access)
        tmp_fmt_dr['last_chn'] = fs_h.FS_DATE_TIME.format(*last_chn_format)
        st_str = f'{{0}} 0x{{0:0{self.table_hex_width}X}}'
        tmp_fmt_dr['start_cluster'] = st_str.format(infos.start_cluster)
        tmp_fmt_dr['f_size'] = infos.file_size
        self.__print_to([fs_h.OUT_DIR.format(**tmp_fmt_dr)], display)

    @staticmethod
    def __vfat(vfat_entry):
        """
        read long file name
        """

        sequence_nr = struct.unpack_from(fs_h.U_CHAR_LIT, vfat_entry, 0x00)[0]
        # bit 5: 0; bits 4-0: number 0x01..0x14 (0x1F), DELETED entry: 0xE5)
        nr = sequence_nr & fs_h.BIT_0_TO_5
        first_pyhs_entry = sequence_nr & fs_h.BIT_6 # last logical, first physical LFN entry

        # name chars (five UCS-2 characters)
        first_name_chars = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(10), vfat_entry, 0x01)[0]

        # attributes (always 0x0F)
        # not used

        # Type (always 0x00 for VFAT LFN, other values reserved for future use
        # vfat_type = struct.unpack_from(fs_h.U_CHAR_LIT, vfat_entry, 0x0C)[0]

        # checksum of DOS file name
        chk_sum = struct.unpack_from(fs_h.U_CHAR_LIT, vfat_entry, 0x0D)[0]

        # name chars (six UCS-2 characters)
        sec_name_chars = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(12), vfat_entry, 0x0E)[0]

        # girst cluster (always 0x0000)
        # not used

        # name characters (two UCS-2 characters)
        third_name_chars = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(4), vfat_entry, 0x1C)[0]

        # erase waste chars and convert in matching encoding
        first_chars_tmp = first_name_chars.translate(None, fs_h.FILL_VAL)
        sec_chars_tmp = sec_name_chars.translate(None, fs_h.FILL_VAL)
        third_chars_tmp = third_name_chars.translate(None, fs_h.FILL_VAL)
        first_chars_tmp = first_chars_tmp.decode(fs_h.ENC_UTF_16, fs_h.ENC_ERRORS).rstrip(fs_h.END_CHAR)
        sec_chars_tmp = sec_chars_tmp.decode(fs_h.ENC_UTF_16, fs_h.ENC_ERRORS).rstrip(fs_h.END_CHAR)
        third_chars_tmp = third_chars_tmp.decode(fs_h.ENC_UTF_16, fs_h.ENC_ERRORS).rstrip(fs_h.END_CHAR)

        long_file_name = ''.join([first_chars_tmp, sec_chars_tmp, third_chars_tmp])

        vf = namedtuple('Vfat', ['nr', 'first_pyhs_entry', 'chk_sum', 'long_file_name'])
        vfat = vf(nr, first_pyhs_entry, chk_sum, long_file_name)
        return vfat

    def save_files(self, root_name, display=True):
        """
        save files / dirs from image to a dir
        """
        ausgabe = [fs_h.FILES_SAVE]
        for f in self.file_list:
            tmp_name = f.dir_name[:] # name for first dir
            tmp_name.insert(0, root_name)
            dir_name = '/'.join(tmp_name)

            if not os.path.exists(dir_name): # sub dir create
                os.makedirs(dir_name)

            if f.long_file_name:
                save_file_name = f'{dir_name}/{f.long_file_name}'
            else:
                save_name = f.file_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS).strip()
                save_ext = f.ext_name.decode(fs_h.ENC_CP850, fs_h.ENC_ERRORS).strip()
                save_file_name = f'{dir_name}/{save_name}.{save_ext}'

            save_file = open(save_file_name, 'wb')

            # read cluster and save as file
            for fc in self.__fat_to_cluster(f.start_cluster):
                file_cluster = self.__read_sektor(
                    self.cts(fc, self.first_data_sector, self.root_dir_sectors, self.sect_per_clust), self.cluster_size)
                save_file.write(file_cluster)

            save_file.close()

            """file_create_date_time is not in use"""
            # if not f.cd[1] or not f.cd[2]:
            #     file_create_date_time = time.localtime()
            # else:
            #     file_create_date_time = time.strptime(
            #         '%d %02d %02d %02d %02d %02d' % (f.cd[0], f.cd[1], f.cd[2], f.ct[0], f.ct[1], f.ct[2]),
            #         '%Y %m %d %H %M %S')

            if not f.lad[1] or not f.lad[2]:
                file_last_access_date = time.localtime()
            else:
                fla_str = f'{f.lad[0]:d} {f.lad[1]:02d} {f.lad[2]:02d}'
                file_last_access_date = time.strptime(fla_str, '%Y %m %d')

            if not f.lcd[1] or not f.lcd[2]:
                file_last_change_date_time = time.localtime()
            else:
                flc_str = f'{f.lcd[0]:d} {f.lcd[1]:02d} {f.lcd[2]:02d} {f.lct[0]:02d} {f.lct[1]:02d} {f.lct[2]:02d}'
                file_last_change_date_time = time.strptime(flc_str, '%Y %m %d %H %M %S')

            # date and time change for saved files (last change)
            os.utime(save_file_name, (time.mktime(file_last_access_date), time.mktime(file_last_change_date_time)))

            ausgabe.extend([' ', save_file_name, fs_h.FILE_SAVED])
        self.__print_to(ausgabe, display)


if __name__ == '__main__':
    from helper import fs_helper as fs_h

    img_name = 'test.img'
    fat = Fat(img_name, ''.join([img_name, '.save_files.log']))
    fat.start(0)
    fat.print_bootsektor_info()
    fat.read_print_fat(display=True)
    fat.get_files(display=True)
    fat.save_files('.'.join(img_name.split('.')[:-1]))
    fat.close()
