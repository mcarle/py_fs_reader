#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import struct
import sys
from collections import namedtuple

if __name__ != '__main__':
    from fs_libs.helper import fs_helper as fs_h

class Mbr(object):
    '''
    Classical generic MBR
    '''
    def __init__(self, datei_name):
        self.bootloader = None
        self.disk_sig = 0
        self.raw_partitiontable = None
        self.partitiontable = []
        self.mbr_sig = 0
        try:
            self.img_file = open(datei_name, 'rb')
        except:
            print(fs_h.IMG_FILE_ERR)
            sys.exit(1)

    def read_mbr(self):
        first_sektor = self.img_file.read(fs_h.SECT_SIZE)
        self.bootloader = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(fs_h.MBR_BOOTLOADER_SIZE), first_sektor, fs_h.MBR_BOOTLOADER_OFFSET)[0]
        self.disk_sig = struct.unpack_from(fs_h.U_SHORT_LIT, first_sektor, fs_h.MBR_DISK_SIG_OFFSET)[0]
        self.raw_partitiontable = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(fs_h.MBR_RAW_PART_TABLE_SIZE), first_sektor, fs_h.MBR_RAW_PART_TABLE_OFFSET)[0]
        for i in range(0, fs_h.SIZE_ALL_PART_TABLES, fs_h.SIZE_ONE_PART_TABLE):
            p = self.raw_partitiontable[i:i + fs_h.SIZE_ONE_PART_TABLE]
            pe_lst = ['booteble',
                      'chs_first',
                      'part_typ',
                      'chs_last',
                      'start_sect',
                      'num_sect']
            partt_entry = namedtuple('Partt_entry', pe_lst)
            pe_dict = {}
            pe_dict['booteble'] = struct.unpack_from(fs_h.U_CHAR_LIT, p, fs_h.BOOTEBLE_OFFSET)[0]
            pe_dict['chs_first'] = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(fs_h.CHS_FIRST_SIZE), p, fs_h.CHS_FIRST_OFFSET)[0]
            pe_dict['part_typ'] = struct.unpack_from(fs_h.U_CHAR_LIT, p, fs_h.PART_TYP_OFFSET)[0]
            pe_dict['chs_last'] = struct.unpack_from(fs_h.CHAR_ARRAY_LIT(fs_h.CHS_LAST_SIZE), p, fs_h.CHS_LAST_OFFSET)[0]
            pe_dict['start_sect'] = struct.unpack_from(fs_h.U_INT_LIT, p, fs_h.START_SECT_OFFSET)[0]
            pe_dict['num_sect'] = struct.unpack_from(fs_h.U_INT_LIT, p, fs_h.NUM_SECT_OFFSET)[0]
            tmp = partt_entry(**pe_dict)
            self.partitiontable.append(tmp)

        self.mbr_sig = struct.unpack_from(fs_h.U_SHORT_LIT, first_sektor, 0x1FE)[0]

    def print_mbr(self):
        dsp_lst = {}
        dsp_lst['mbr_sig'] = self.mbr_sig
        dsp_lst['disk_sig'] = self.disk_sig
        print_lst = [fs_h.OUT_MBR.format(**dsp_lst)]

        for i in self.partitiontable:
            partt_dict = {}
            pt = (fs_h.PART_TYPES.get(i.part_typ, fs_h.MBR_IDF_ERR), i.part_typ)
            bf = ((fs_h.YES if i.booteble == fs_h.MBR_IS_BOOTEBLE else fs_h.NO), i.booteble)
            partt_dict['part_typ'] = '{0} (0x{1:02X})'.format(*pt)
            partt_dict['boot'] = '{0} (0x{1:02X})'.format(*bf)
            partt_dict['chs_first'] = i.chs_first
            partt_dict['chs_last'] = i.chs_last
            partt_dict['start_sect'] = i.start_sect
            partt_dict['lba_met'] = i.num_sect
            print_lst.extend(fs_h.OUT_PARTT.format(**partt_dict))

        print_str = ''.join(print_lst)
        print(print_str, end='')

    def close(self):
        self.img_file.close()


if __name__ == '__main__':
    from helper import fs_helper as fs_h

    img_name = 'test.img'
    mbr = Mbr(img_name)
    mbr.read_mbr()
    mbr.print_mbr()
    mbr.close()
