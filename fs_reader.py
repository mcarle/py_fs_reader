#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import argparse
import os.path

from fs_libs import mbr, fat
from fs_libs.helper import fs_helper as fs_h


def mbr_start(args):
    # file exists check
    if not os.path.exists(args.file):
        print(fs_h.NO_IMG_FILE)
        return

    m = mbr.Mbr(args.file)
    m.read_mbr()
    m.print_mbr()
    m.close()

def fat_start(args):
    if not os.path.exists(args.file):
        print(fs_h.NO_IMG_FILE)
        return

    # wie viel angezeigt werden soll
    verb_save_files = verb_get_files = verb_read_fat = False

    if args.verbose == 1:
        verb_read_fat = True
    elif args.verbose == 2:
        verb_get_files = verb_read_fat = True
    elif args.verbose == 3:
        verb_get_files = verb_save_files = verb_read_fat = True

    log_fn = ''
    if args.log:
        if not args.log:
            log_fn = ''.join([args.log, '.log'])
        else:
            log_fn = ''.join([args.file, '.log'])

    getfat = fat.Fat(args.file, log_fn)
    getfat.start(args.offset)
    getfat.print_bootsektor_info()
    getfat.read_print_fat(verb_read_fat)
    getfat.get_files(verb_get_files)
    if args.out:
        if not args.out:
            out_dn = args.out
        else:
            out_dn = '.'.join(args.file.split('.')[:-1])
        getfat.save_files(out_dn, verb_save_files)
    getfat.close()


def fse_start(args):
    print('use --help for more info')


def main():
    dscr = 'filesystem info / extract'

    h_file = 'image file'
    h_log = 'log file'
    h_out = 'save files/directories to OUT directory'
    h_offset = 'offset to the Image'
    h_verbose = 'increase output verbosity'

    parser = argparse.ArgumentParser(description=dscr)
    parser.set_defaults(func=fse_start)
    subparsers = parser.add_subparsers()

    parser_mbr = subparsers.add_parser('mbr')
    parser_mbr.add_argument('file', help=h_file)
    parser_mbr.set_defaults(func=mbr_start)

    parser_fat = subparsers.add_parser('fat')
    parser_fat.add_argument('file', help=h_file)
    parser_fat.add_argument('--log', '-l', help=h_log, nargs='?', const=True)
    parser_fat.add_argument('--out', '-o', help=h_out, nargs='?', const=True)
    parser_fat.add_argument('--offset', '-O', help=h_offset, type=int, default=0)
    parser_fat.add_argument('--verbose', '-v', help=h_verbose, choices=range(1, 4), type=int)
    parser_fat.add_argument('--version', '-V', action='version', version='%(prog)s 0.1')
    parser_fat.set_defaults(func=fat_start)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
